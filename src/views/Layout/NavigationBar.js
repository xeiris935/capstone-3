import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem
} from 'reactstrap';

const NavigationBar = () => {

	const [isOpen, setIsOpen] = useState(false);
	const [user,setUser] = useState("");

	const toggle = () => {
		setIsOpen(!isOpen);
	}

	const logout = () =>{
		sessionStorage.token = "";
	}

	useEffect(()=>{
		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user);
			setUser(user);
		}
    }, [])
	
	return(
		<React.Fragment>
			<Navbar className="navbar-color" dark expand="md">
				<NavbarBrand href="/" className="navbar-website-name">Codebase</NavbarBrand>
				<NavbarToggler onClick={toggle}/>
				<Collapse isOpen={isOpen} navbar className="navbar-toggler">
					<Nav className="mr-auto navbar-test" navbar>
			            <NavItem className="px-1">
			            	<Link to="/" className="navbar-link">Home</Link>
			            </NavItem>
			            <NavItem className="px-1">
			            	<Link to="/courses" className="navbar-link">Courses</Link>
			            </NavItem>
			            {user ?
			            <React.Fragment>
				            <NavItem className="px-1">
				            	<Link to="/classes" className="navbar-link">Classes</Link>
				            </NavItem>
				            <NavItem className="px-1">
				            	<Link to="/login"
				            		className="navbar-link"
									onClick={logout}
								>Logout</Link>
				            </NavItem>
				            <NavItem className="px-1">
				            	<Link to="#" className="navbar-link">Hello {user.firstName}</Link>
				            </NavItem>
			            </React.Fragment>
			            : ""}
			            {!user ?
			            <React.Fragment>
				            <NavItem className="px-1">
				            	<Link to="/login" className="navbar-link">Login</Link>
				            </NavItem>
				            <NavItem className="px-1">
				            	<Link to="/register" className="navbar-link">Register</Link>
				            </NavItem>
			            </React.Fragment>
			            : ""}
			        </Nav>
				</Collapse>
			</Navbar>
		</React.Fragment>
	)
}

export default NavigationBar;