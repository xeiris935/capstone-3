import React from 'react';
import {Button} from 'reactstrap'
import Image from 'react-bootstrap/Image'

const CourseCard = (props) => {

	const course = props.course

	const handleRedirectLogin = () => {
		window.location.replace('#/login');
	}

	return(
		<React.Fragment>
			<div className="col-lg-3 col-md-3 py-3 mx-3 my-3 text-center course-card">
				<h2 className="card-header">{course.language}</h2>
				<p className="card-text">{course.description}</p>
				<Image 
					src={process.env.PUBLIC_URL + course.image}
					className="card-image"
				/>
				<h3 className="card-text">{course.price} PHP</h3>
				{!props.student ?
				<Button
					className="success"
					onClick={handleRedirectLogin}
				>Book a class</Button>
				:
				<Button
					className="success"
					onClick={()=>props.handleClassBooking(course)}
				>Book a class</Button>
				}
			</div>
		</React.Fragment>
	)
}

export default CourseCard;